# gsumchecker 
A CLI checksum comparison tool. It will calculate the checksum of the opened file or text and compare it with the provided one.
## Supported checksum formats
* md5
* sha1

## Dependencies

* https://github.com/therecipe/qt (required to build a GUI)

## How to build
1. `$ git clone https://gitlab.com/basketdou/gsumchecker`
(Clone the repo by executing the command in terminal)

2. `$ go build main.go` 
(compile the tool)

3. `$ cd qsumchecker && go build qmain.go` 
(compile the Qt GUI (alpha))

4. `$ cd gsumcheck && go build`
(compile the Fyne GUI (alpha) (recommended))

5. `$ strip qsumchecker`
(decrease the size of the executable) (optional)
